<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $title = 'Listado de usuarios';
        return view('users.users', compact('title', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->input('company') === null){
            $company = 0;
        } else {
            $company = $request->input('company');
        }

        User::create([
                'name' =>  $request->input('name'),
                'email' => $request->input('email'),
                'password' => Hash::make($request->input('password')),
                'id_company' => $company,
            ]);
        $users = User::all();

        session()->flash('message', 'El usuario se ha creado correctamente');
        session()->flash('alert-class', 'alert-success');
        //return view('users.users', compact('users'));
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */

    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    
    public function edit(Company $company)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request, $id)
    {
//        User::find($id)->update($request->all());

        User::find($id)->update([
            'name' =>  $request->input('username'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
        ]);

        session()->flash('message', 'El Usuario se ha actualizado correctamente');
        session()->flash('alert-class', 'alert-success');
        return redirect()->route('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();

        session()->flash('message', 'El Usuario se ha eliminado correctamente');
        session()->flash('alert-class', 'alert-success');
        return redirect()->route('users');

    }



    public function logs(Request $request){

        // $email = $request->input('email');
        // $password = Hash::make($request->input('password'));

        $credentials = array(
            'email' => $request->input('email'),
            'password'=> $request->input('password')
        );

        //$data = DB::select('SELECT * FROM users WHERE email=? and password=?', [$email, $password]);

        //dd($email . " - " . $password);

        if (Auth::attempt($credentials)) {
            echo "You are Logging";
            // print_r($data);
        } else {
            echo "Don't Loggin";
        }

    }
    
    
    
    /**
     * Api Functions
     */
    public function catalogos(Request $request){
        // dd($request->email);
        
        $out= DB::table('catalogues')
            ->join('companies','companies.id','=','catalogues.company_id')
            ->where('companies.email','=',$request->email)
            ->select('catalogues.name')
            ->get();
        return $out;
        //return $this->sendResponse($catalogue->toArray(), 'Products retrieved successfully.');
        // return redirect()->route('catalogue.index');
    }



    

}

