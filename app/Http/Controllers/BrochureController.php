<?php

namespace App\Http\Controllers;

use App\User;
use App\Company;
use App\Brochure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BrochureController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brochure = DB::table('brochures')
            ->join('companies','companies.id','=','brochures.company_id')
            ->select('brochures.*','companies.name AS shop')
            ->get();
        $companies = Company::all();
        return view('brochure.index', compact('brochure', 'companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('folleto')) {
            $file = $request->file('folleto');
            $filename = $file->getClientOriginalName();
            $filenameNew = public_path() . "/folletos/" . $filename;

            if (file_exists($filenameNew)){
                session()->flash('message', 'Ya existe un folleto con este nombre, verifiquelo y vuelva a subirlo nuevamente');
                session()->flash('alert-class', 'alert-danger');
                return redirect()->route('brochure.index');
            }else{
                $file->move(public_path() . '/folletos/', $filename);

                Brochure::create([
                    'name' => $filename,
                    'company_id' => $request->input('name'),
                ]);

                session()->flash('message', 'Se ha cargado exitosamente el folleto.');
                session()->flash('alert-class', 'alert-success');
                return redirect()->route('brochure.index');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brochure  $brochure
     * @return \Illuminate\Http\Response
     */
    public function show(Brochure $brochure)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Brochure  $brochure
     * @return \Illuminate\Http\Response
     */
    public function edit(Brochure $brochure)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brochure  $brochure
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brochure $brochure)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brochure  $brochure
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Brochure::find($id)->delete();

        session()->flash('message', 'El Folleto se ha eliminado correctamente');
        session()->flash('alert-class', 'alert-success');
        return redirect()->route('brochure.index');
    }
}
