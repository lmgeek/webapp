<?php

namespace App\Http\Controllers;

use App\User;
use App\Company;
use App\Catalogue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CatalogueController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
  /*  public function __construct()
    {
        $this->middleware('auth');
    }
*/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $catalogue = Catalogue::all();
        $catalogue = DB::table('catalogues')
            ->join('companies','companies.id','=','catalogues.company_id')
            ->select('catalogues.*','companies.name AS shop')
            ->get();
        $companies = Company::all();
        return view('catalogue.index', compact('catalogue', 'companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('catalogo')) {
            $file = $request->file('catalogo');
            $filename = $file->getClientOriginalName();
            $filenameNew = public_path() . "/catalogos/" . $filename;

            $c = DB::table('catalogues')
            ->where('company_id','=',$request->input('name'))
            ->orderBy('created_at','desc')
            ->first();

            if ($c == null) {
                if (file_exists($filenameNew)){
                    \File::delete(public_path('catalogos/'.$filename));
                    // if(!file_exists($filenameNew) || !is_uploaded_file($filenameNew)) { 
                    //     session()->flash('message', 'No se ha cargar el catálogo, intente nuevamente');
                    //     session()->flash('alert-class', 'alert-danger'); 
                    //     return redirect()->route('catalogue.index');
                    // } 
                    Catalogue::create([
                        'name' => $filename,
                        'company_id' => $request->input('name'),
                    ]);
                    session()->flash('message', 'Se ha cargado exitosamente el catálogo');
                    session()->flash('alert-class', 'alert-success');
                    return redirect()->route('catalogue.index');
                }else{
                    $file->move(public_path() . '/catalogos/', $filename);
                    // if(!file_exists($filenameNew) || !is_uploaded_file($filenameNew)) { 
                    //     session()->flash('message', 'No se ha cargar el catálogo, intente nuevamente');
                    //     session()->flash('alert-class', 'alert-danger'); 
                    //     return redirect()->route('catalogue.index');
                    // } 

                    Catalogue::create([
                        'name' => $filename,
                        'company_id' => $request->input('name'),
                    ]);

                    session()->flash('message', 'Se ha cargado exitosamente el catálogo.');
                    session()->flash('alert-class', 'alert-success');
                    return redirect()->route('catalogue.index');
                }
            } else {
                if (file_exists($filenameNew)){
                   \File::delete(public_path('catalogos/'.$filename));
                   // if(!file_exists($filenameNew) || !is_uploaded_file($filenameNew)) { 
                   //      session()->flash('message', 'No se ha cargar el catálogo, intente nuevamente');
                   //      session()->flash('alert-class', 'alert-danger'); 
                   //      return redirect()->route('catalogue.index');
                   //  } 

                    $cat = Catalogue::find($c->id);
                    $cat->name = $filename;
                    $cat->company_id = $request->input('name');
                    $cat->save();

                    session()->flash('message', 'Se ha cargado exitosamente el catálogo');
                    session()->flash('alert-class', 'alert-success');
                    return redirect()->route('catalogue.index');
                }else{
                    $file->move(public_path() . '/catalogos/', $filename);
                    // if(!file_exists($filenameNew) || !is_uploaded_file($filenameNew)) { 
                    //     session()->flash('message', 'No se ha cargar el catálogo, intente nuevamente');
                    //     session()->flash('alert-class', 'alert-danger'); 
                    //     return redirect()->route('catalogue.index');
                    // } 

                    $cat = Catalogue::find($c->id);
                    $cat->name = $filename;
                    $cat->company_id = $request->input('name');
                    $cat->save();

                    session()->flash('message', 'Se ha cargado exitosamente el catálogo.');
                    session()->flash('alert-class', 'alert-success');
                    return redirect()->route('catalogue.index');
                }
            }

            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Catalogue  $catalogue
     * @return \Illuminate\Http\Response
     */
    public function show(Catalogue $catalogue)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Catalogue  $catalogue
     * @return \Illuminate\Http\Response
     */
    public function edit(Catalogue $catalogue)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Catalogue  $catalogue
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Catalogue $catalogue)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Catalogue  $catalogue
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Catalogue::find($id)->delete();

        session()->flash('message', 'El catálogo se ha eliminado correctamente');
        session()->flash('alert-class', 'alert-success');
        return redirect()->route('catalogue.index');
    }


    /**
     * Api Functions
     */
    public function catalogos (Request $request){
        dd($request);
        $catalogue = Catalogue::all();
        /*B::table('catalogues')
            ->join('companies','companies.id','=','catalogues.company_id')
            ->select('catalogues.*','companies.name AS shop')
            ->get();*/

        return $this->sendResponse($catalogue->toArray(), 'Products retrieved successfully.');
        // return redirect()->route('catalogue.index');
    }
}
