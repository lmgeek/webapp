<?php

namespace App\Http\Controllers;

use App\User;
use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function users()
    {
        //$users = DB::table('users')->get();
        // $users = User::all();
        $users = DB::table('users')
            ->join('companies','companies.id','=','users.id_company')
            ->select('users.*','companies.name AS company')
            ->get();
        $companies = Company::all();
//        return view('users.index')
//            ->with('users', User::all())
//            ->with('title', 'Listado de usuarios');
        return view('users.users', compact('users','companies'));
    }

    public function admin()
    {
        $users = DB::table('users')
            ->where('id_company','=','0')
            ->get();

        return view('users.admin', compact('users'));
    }





    public function file(){
         return view('users');
    }

    public function file2(Request $request){
        if($request->hasFile('avatar')){
            $file = $request->file('avatar');
            $name = time().$file->getClientOriginalName();
            $file->move(public_path().'/images/company', $name);

            return $name;
        }


        return $request;
    }


}
