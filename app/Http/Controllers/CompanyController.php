<?php

namespace App\Http\Controllers;

use Excel;
use App\User;
use App\Company;
use App\Exports\UserExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;

class CompanyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $companies = Company::all();

        // $companies = DB::table('companies')
        //     ->join('users','users.id_company','=','companies.id')
        //     ->select('companies.*','users.name AS username')
        //     ->get();
//        $shop = Company::orderby('created_at','DESC')->take(1)->get();
//         print_r($shop[0]['id']); dd();

        return view('companies', compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
//        return "Hola";
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $existUser = DB::table('users')->where('email','=',$request->input('email'))->get();
        if(count($existUser)>0){
            session()->flash('message', 'el usuario para esta tienda ya existe, verifiquelo e intente nuevamente');
            session()->flash('alert-class', 'alert-danger');
            return redirect()->route('shop.index'); 
        }

        if($request->hasFile('avatar')){
            $file = $request->file('avatar');
            $filename = time().$file->getClientOriginalName();
            $file->move(public_path().'/images/company/', $filename);

            $c = Company::all();
            $c2 = User::all();

            foreach($c as $co){
                if($request->name == $co->name){
                    session()->flash('message', 'La Tienda ya existe, no se pudo registrar');
                    session()->flash('alert-class', 'alert-danger');
                    return redirect()->route('shop.index');
                } elseif($request->email == $co->email) {
                    session()->flash('message', 'El correo de Tienda  ya existe, no se pudo registrar');
                    session()->flash('alert-class', 'alert-danger');
                    return redirect()->route('shop.index');
                } else {
                    foreach($c2 as $u){
                        if($request->name == $u->email){
                            session()->flash('message', 'El Usuario para la Tienda ya existe, no se pudo registrar');
                            session()->flash('alert-class', 'alert-danger');
                            return redirect()->route('shop.index');
                        }
                    }
                }
            }

            $company = new Company();
            $company->name = $request->input('name');
            $company->email = $request->input('email');
            $company->logo = $filename;
            $company->save();

            /*User::Create($request->all());
            $user = new User();
            $user->name = $request->input('username');
            $user->email = $request->input('email');
            $user->password = Hash::make($request->input('password'));
            $user->save();*/

            $shop = Company::orderby('created_at','DESC')->take(1)->get();
            //dd($shop[0]['id']);
            
            User::create([
                'name' =>  $request->input('name'),
                'email' => $request->input('email'),
                'password' => Hash::make($request->input('password')),
                'id_company' => $shop[0]['id'],
            ]);

            session()->flash('message', 'La Tienda se ha creado exitrosamente');
            session()->flash('alert-class', 'alert-success');
            return redirect()->route('shop.index');
        } else {
            session()->flash('message', 'Ocurrió un error al registrar la Tienda');
            session()->flash('alert-class', 'alert-danger');
            return redirect()->route('shop.index');
        }



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd(Company::find($id));
        Company::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//        dd($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (!empty($request->file('avatar'))){
            if($request->hasFile('avatar')){
                $file = $request->file('avatar');
                $filename = time().$file->getClientOriginalName();
                $file->move(public_path().'/images/company/', $filename);

                Company::find($id)->update([
                    'name' => $request->input('name'),
                    'email' => $request->input('email'),
                    'logo' => $filename,
                ]);

                session()->flash('message', 'La Tienda se ha actualizado exitrosamente');
                session()->flash('alert-class', 'alert-success');
                return redirect()->route('shop.index');
            }
        } else {

            Company::find($id)->update([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
            ]);

            session()->flash('message', 'La Tienda se ha actualizado exitrosamente');
            session()->flash('alert-class', 'alert-success');
            return redirect()->route('shop.index');
        }


//        Company::find($id)->update($request->all());
//
//        session()->flash('message', 'La Tienda se ha actualizado correctamente');
//        session()->flash('alert-class', 'alert-success');
//        return redirect()->route('shop.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $c = Company::find($id);
        $u = DB::table('users')
        ->where('id_company','=',$c->id)
        ->get();

        User::find($u[0]->id)->delete();
        Company::find($c->id)->delete();

        session()->flash('message', 'La Tienda y el usuario relacionado se ha eliminado correctamente');
        session()->flash('alert-class', 'alert-success');
        return redirect()->route('shop.index');
    }



    public function excel()
    {        
        
        $company = Company::all();  
        $users = User::all();  

        return view('reporte', compact('company','users'));
    }
}
