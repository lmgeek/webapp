<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brochure extends Model
{
    protected $fillable = ['name', 'company_id'];
}
