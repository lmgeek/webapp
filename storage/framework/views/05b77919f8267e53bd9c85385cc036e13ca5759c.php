<div class="footer">
  <div class="pull-right">
    <!-- Hecho con <i class="fa fa-heart" style="color: red;"></i> by <a href="//marinluis.com" target="_blank">Luis Marín</a> -->
    Departamento de Informática
  </div>
  <div>
    <strong>&copy; Copyright <?php echo e(date('Y')); ?></strong> Colchones Celta - Camas y Sueños
  </div>
</div>
