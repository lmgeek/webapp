<div id="page-wrapper" class="gray-bg">
  <div class="row border-bottom">
    <nav class="navbar navbar-static-top  " role="navigation" style="margin-bottom: 0">
      <div class="navbar-header">
        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
      </div>
      <ul class="nav navbar-top-links navbar-right">
      
        <li>
         <form id="logout-form" action="<?php echo e(url('logout')); ?>" method="POST" style="display: none;">
           <?php echo e(csrf_field()); ?>

         </form>
         <a href="" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
            <i class="fa fa-sign-out"></i>Cerrar Sesión
         </a>
        </li>
      </ul>
    </nav>
  </div>
  <div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-sm-4">
      <h2><?php echo $__env->yieldContent('content-title', 'Administrador de Catálogos y App Movil'); ?></h2>
      <?php $__env->startSection('breadcrumbs'); ?>
      <ol class="breadcrumb">
        <li>
          <a href="#">App Moviles</a>
        </li>
        <li class="active">
          <strong>Breadcrumb</strong>
        </li>
      </ol>
      <?php echo $__env->yieldSection(); ?>
    </div>
  </div>

  <div class="wrapper wrapper-content">
    <?php echo $__env->yieldContent('content'); ?>
  </div>
  <?php echo $__env->make('inspinia::layouts.main-panel.footer.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>
