<?php $__env->startSection('content'); ?>
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
      <div><h1 class="logo-name"><img src="/images/logo.jpg" alt=""></h1></div>
      <form class="m-t" role="form" method="POST" action="<?php echo e(route('login')); ?>">
       <?php echo e(csrf_field()); ?>

       <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
        <input type="email" class="form-control" placeholder="E-Mail" name="email" value="<?php echo e(old('email')); ?>" required autofocus>
        <?php if($errors->has('email')): ?>
            <span class="help-block">
                <strong><?php echo e($errors->first('email')); ?></strong>
            </span>
        <?php endif; ?>
       </div>
       <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
        <input type="password" class="form-control" placeholder="Password" name="password" required>
        <?php if($errors->has('password')): ?>
            <span class="help-block">
                <strong><?php echo e($errors->first('password')); ?></strong>
            </span>
        <?php endif; ?>
       </div>
       
       <button type="submit" class="btn btn-primary block full-width m-b">Iniciar Sesión</button>

       <!--<a href="<?php echo e(route('password.request')); ?>"><small>Olvide mi Contraseña</small></a>
       <p class="text-muted text-center"><small>¿No tienes una cuenta?</small></p>
       <a class="btn btn-sm btn-white btn-block" href="<?php echo e(route('register')); ?>">Crear Cuenta</a>-->
      </form>
      <p class="m-t"> &copy; <?php echo e(date('Y')); ?> <strong>Administrador de Catálogos <br>y APP Móvil<!--</strong> <br><small>Create with <i class="fa fa-heart" style="color: red"></i> by <a href="//marinluis.com">Luis Marín</a></small>--></p>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('inspinia::layouts.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>