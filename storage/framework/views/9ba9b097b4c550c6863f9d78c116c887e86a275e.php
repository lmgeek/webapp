<?php
/**
 * Created by PhpStorm.
 * User: luismarin
 * Date: 2019-02-03
 * Time: 14:08
 */

?>


<?php $__env->startSection('title', 'Usuarios'); ?>
<?php $__env->startSection('content'); ?>
    <?php if(Session::has('message')): ?>
        <p class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(Session::get('message')); ?></p>
    <?php endif; ?>
    <div>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#crear">
            Agregar Usuario
        </button>
    </div>
    <h1>Listado de usuarios</h1>
    <table class="table table-bordered">
        <thead class="text-center">
        <tr>
            <th>Nombre</th>
            <th>Usuario</th>
            <th>Empresa</th>
            <th>Acciones</th>
        </tr>
        </thead>
    <?php $__empty_1 = true; $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>
                <tr>
                    <td><?php echo e($user->name); ?></td>
                    <td><?php echo e($user->email); ?></td>
                    <td><?php echo e($user->company); ?></td>
                    <td class="text-center">
                        <div class="col-md-6">
                            <button type="button" class="btn btn-xs" data-toggle="modal" data-target="#<?php echo e($user->id); ?>"><span class="fa fa-edit"></span></button>
                        </div>
                       <div class="col-md-6">
                            <form action="<?php echo e(action('UserController@destroy', $user->id)); ?>" method="post"
                                  onclick="return confirm('Seguro que desea eliminar el usuairo?')">
                                <?php echo e(csrf_field()); ?>

                                <input name="_method" type="hidden" value="DELETE">
                                <button class="btn btn-danger btn-xs" type="submit">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </button>
                            </form>

                        </div>
                    </td>
               </tr>

           

            <div class="modal inmodal" id="<?php echo e($user->id); ?>" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content animated bounceInRight">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                        class="sr-only">Close</span></button>
                            <i class="fa fa-user modal-icon"></i>
                            <h4 class="modal-title">Editar Usuario</h4>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="/users/<?php echo e($user->id); ?>" enctype="multipart/form-data"  >
                                <?php echo e(csrf_field()); ?>

                                <div class="form-group">
                                    <label for="username">Nombre Usuario</label>
                                    <input type="text" id="username" placeholder="Ingrese Nombre de la Usuario" class="form-control" name="username" value="<?php echo e($user->name); ?>" >
                                </div>
                                <div class="form-group">
                                    <label for="email">Correo Usuario</label>
                                    <input type="email" id="email" placeholder="Ingrese Correo del Usuario" class="form-control" name="email" value="<?php echo e($user->email); ?>" >
                                </div>
                                <div class="form-group">
                                    <label for="password">Contraseña</label>
                                    <input type="password" id="password" placeholder="Ingrese Contraseña del Usuario" class="form-control" name="password" required>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
            <h4>No hay usuarios registrados.</h4>
        <?php endif; ?>
    </table>

    

            <div class="modal inmodal" id="crear" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content animated bounceInRight">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                        class="sr-only">Close</span></button>
                            <i class="fa fa-user modal-icon"></i>
                            <h4 class="modal-title">Crear Usuario</h4>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="<?php echo e(route('admin.create')); ?>" enctype="multipart/form-data"  >
                                <?php echo e(csrf_field()); ?>

                                <div class="form-group">
                                    <label for="username">Nombre Usuario</label>
                                    <input type="text" id="username" placeholder="Ingrese Nombre de la Usuario" class="form-control" name="name" value="" >
                                </div>
                                <div class="form-group">
                                    <label for="email">Correo Usuario</label>
                                    <input type="email" id="email" placeholder="Ingrese Correo del Usuario" class="form-control" name="email" value="" >
                                </div>
                                <div class="form-group">
                                    <label for="password">Contraseña</label>
                                    <input type="password" id="password" placeholder="Ingrese Contraseña del Usuario" class="form-control" name="password" required>
                                </div>
                                <div class="form-group">
                                    <label for="company">Tienda</label>
                                    <select name="company" id="company"class="form-control" required>
                                        <option value="">Seleccione una tienda</option>
                                        <?php $__currentLoopData = $companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($c->id); ?>"><?php echo e($c->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('sidebar'); ?>
    ##parent-placeholder-19bd1503d9bad449304cc6b4e977b74bac6cc771##
<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>