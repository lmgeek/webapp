<?php
/**
 * Created by PhpStorm.
 * User: luismarin
 * Date: 2019-02-03
 * Time: 14:46
 */
?>



<?php $__env->startSection('title', 'Catálogos'); ?>


<?php $__env->startSection('content'); ?>

    <?php if(Session::has('message')): ?>
        <!-- <div class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?> alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong><?php echo e(Session::get('message')); ?></strong>
        </div> -->
        <p class="alert <?php echo e(Session::get('alert-class', 'alert-info')); ?>"><?php echo e(Session::get('message')); ?></p>
    <?php endif; ?>
    <div>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
            <i class="fa fa-upload"></i> Subir Catálogo
        </button>

    </div>
    <h1>Catálogos</h1>

    <table class="table table-bordered">
        <thead class="text-center">
        <tr>
            <th>Tienda</th>
            <th>Catálogo Fecha</th>
            <th>Acción</th>
        </tr>
        </thead>
        <?php $tienda_id = 0; ?>
        <?php $__empty_1 = true; $__currentLoopData = $catalogue; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $c): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); $__empty_1 = false; ?>

            <?php if($c->company_id != $tienda_id): ?>

            <tr>
                <td><?php echo e($c->shop); ?></td>
                <td><?php echo e(date('d-m-Y / H:m:s',strtotime($c->updated_at))); ?></td>
                <td>
                    <div class="col-md-6">

                        <button class="btn btn-xs btn-primary" data-toggle="modal" data-target="#<?php echo e($c->id); ?>"><i class="fa fa-eye"></i></button>
                        
                    </div>
                    <div class="col-md-6">
                        <form action="<?php echo e(action('CatalogueController@destroy', $c->id)); ?>" method="post"
                              onclick="return confirm('Seguro que desea eliminar el catálogo?')">
                            <?php echo e(csrf_field()); ?>

                            <input name="_method" type="hidden" value="DELETE">

                            <button class="btn btn-danger btn-xs" type="submit"><span
                                        class="glyphicon glyphicon-trash"></span></button>
                        </form>
                    </div>
                </td>
                </td>
                
            </tr>

            
            <div class="modal inmodal" id="<?php echo e($c->id); ?>" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content animated bounceInRight">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                        class="sr-only">Close</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <iframe src="<?php echo e(url('/catalogos', $c->name)); ?>#zoom=100&view=fitH" frameborder="0" width="100%" height="400px"></iframe>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">salir</button>
                        </div>
                    </div>
                </div>
            </div>
            
            <?php endif; ?>

            <?php $tienda_id = $c->company_id ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); if ($__empty_1): ?>
            <h4>No hay catalogos cargados.</h4>
        <?php endif; ?>
    </table>



    <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                    <i class="fa fa-newspaper-o modal-icon"></i>
                    <h4 class="modal-title">Subir Catálogo</h4>
                </div>
                <div class="modal-body">
                    <form method="POST" action="<?php echo e(route('catalogue.store')); ?>" enctype="multipart/form-data">
                        <?php echo e(csrf_field()); ?>

                        <div class="form-group">
                            <label for="name">Tienda</label>
                            <select name="name" id="name" class="form-control" required>
                                <option value="">------------  Seleccione una tienda  -----------</option>
                                <?php $__currentLoopData = $companies; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shop): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($shop->id); ?>"><?php echo e($shop->name); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="file" name="catalogo" value="" required>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>


<?php $__env->stopSection(); ?>

<?php $__env->startSection('sidebar'); ?>
    ##parent-placeholder-19bd1503d9bad449304cc6b4e977b74bac6cc771##
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>