<!DOCTYPE html>
<html lang="<?php echo $__env->yieldContent('lang', config('app.locale', 'en')); ?>">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Luis Marin">

  <title><?php echo $__env->yieldContent('title', config('app.name', 'Celta Camas y Sue&ntilde:os')); ?></title>

  <!-- CSRF Token -->
  <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

  <!-- Styles -->
  <?php $__env->startSection('styles'); ?>
  <link href="/webapp<?php echo e(mix('/css/inspinia.css')); ?>" rel="stylesheet">
  <link href="/webapp<?php echo e(asset('/css/style.css')); ?>" rel="stylesheet">
  <!--  Toastr -->
  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


  <?php echo $__env->yieldSection(); ?>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.3/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js">
  </script>
  <script scr="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.2/js/toastr.min.js">
  </script>
  <![endif]-->
  <?php echo $__env->yieldPushContent('head'); ?>
</head>

<body class="body-small <?php echo e(config('inspinia.skin', '')); ?>">
  <div id="wrapper">
    <?php echo $__env->make('inspinia::layouts.sidebar.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('inspinia::layouts.main-panel.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </div>

  <?php $__env->startSection('scripts'); ?>
      <script
          src="https://code.jquery.com/jquery-3.3.1.min.js"
          integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
          crossorigin="anonymous"></script>
      <script src="/webapp<?php echo e(mix('/js/manifest.js')); ?>" charset="utf-8"></script>
      <script src="/webapp<?php echo e(mix('/js/vendor.js')); ?>" charset="utf-8"></script>
      <script src="/webapp<?php echo e(mix('/js/inspinia.js')); ?>" charset="utf-8"></script>
	<?php echo $__env->yieldSection(); ?>
	<?php echo $__env->yieldPushContent('body'); ?>
</body>

</html>
