<style>
    img[src*="https://cdn.000webhost.com/000webhost/logo/footer-powered-by-000webhost-white2.png"] { display: none; }
</style>

<?php if(auth()->check()): ?>
<?php $__env->startSection('user-avatar', 'https://www.gravatar.com/avatar/' . md5(auth()->user()->email) . '?d=mm'); ?>
<?php $__env->startSection('user-name', auth()->user()->name); ?>
<?php endif; ?>

<?php $__env->startSection('breadcrumbs'); ?>
<?php echo $__env->make('inspinia::layouts.main-panel.breadcrumbs', [
  'breadcrumbs' => [
    (object) [ 'title' => ' ', 'url' => route('home') ]
  ]
], array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('sidebar-menu'); ?>
  <ul class="nav metismenu" id="side-menu" style="padding-left:0px;">
    <li class="active">
      <a href="<?php echo e(route('home')); ?>"><i class="fa fa-home"></i> <span class="nav-label">Home</span></a>
      <a href="<?php echo e(route('shop.index')); ?>"><i class="fa fa-shopping-bag"></i> <span class="nav-label">Tiendas</span></a>
      <a href="<?php echo e(route('catalogue.index')); ?>"><i class="fa fa-newspaper-o"></i> <span class="nav-label">Cat&aacute;logos</span></a>
      <a href="<?php echo e(route('users')); ?>"><i class="fa fa-users"></i> <span class="nav-label">Usuarios</span></a>
      <a href="<?php echo e(route('admin')); ?>"><i class="fa fa-users"></i> <span class="nav-label">Administradores</span></a>
      
      <a href="<?php echo e(route('report.excel')); ?>"><i class="fa fa-bar-chart"></i> <span class="nav-label">Reportes</span></a>
    </li>
  </ul>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('inspinia::layouts.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>