<?php $__env->startSection('content'); ?>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="ibox float-e-margins">
            <div class="ibox-title"><h5>Bienvenido <?php echo e(auth()->user()->name); ?></h5>
                <div class="ibox-tools"> 
                    <span class="label label-danger pull-right"><b>Fecha y Hora Actual:</b> <?=date('d-m-Y H:i')?></span>
                </div>
            </div>

            <div class="ibox-content">
                <?php if(session('status')): ?>
                    <div class="alert alert-success">
                        <?php echo e(session('status')); ?>

                    </div>
                <?php endif; ?>

                <label>
                    Descarga la Aplicación Movil para distribuidores y ver los catálogos instatáneamente
                </label>

                <a href="https://goo.gl/xdPwev" target="_blank">
                    <img src="/webapp/images/descarga-app-android.png" width="40%">
                </a>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>