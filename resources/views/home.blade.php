@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="ibox float-e-margins">
            <div class="ibox-title"><h5>Bienvenido {{ auth()->user()->name }}</h5>
                <div class="ibox-tools"> 
                    <span class="label label-danger pull-right"><b>Fecha y Hora Actual:</b> <?=date('d-m-Y H:i')?></span>
                </div>
            </div>

            <div class="ibox-content">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <label>
                    Descarga la Aplicación Movil para distribuidores y ver los catálogos instatáneamente
                </label>

                <a href="https://goo.gl/xdPwev" target="_blank">
                    <img src="/webapp/images/descarga-app-android.png" width="40%">
                </a>
            </div>
        </div>
    </div>
</div>
@endsection
