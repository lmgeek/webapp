<?php
/**
 * Created by PhpStorm.
 * User: luismarin
 * Date: 2019-02-03
 * Time: 14:08
 */

?>

@extends('layouts.app')
@section('title', 'Usuarios')
@section('content')
    @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif
    <div>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#crear">
            Agregar Administrador
        </button>
    </div>
    <h1>Listado de Administradores</h1>
    <table class="table table-bordered">
        <thead class="text-center">
        <tr>
            <th>Nombre</th>
            <th>Usuario</th>
            <th>Acciones</th>
        </tr>
        </thead>
    @forelse ($users as $user)
                <tr>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td class="text-center">
                        <div class="col-md-6">
                            <button type="button" class="btn btn-xs" data-toggle="modal" data-target="#{{ $user->id }}"><span class="fa fa-edit"></span></button>
                        </div>
                       
                       <div class="col-md-6">
                            <form action="{{action('UserController@destroy', $user->id)}}" method="post"
                                  onclick="return confirm('Seguro que desea eliminar el usuairo?')">
                                {{csrf_field()}}
                                <input name="_method" type="hidden" value="DELETE">
                                <button class="btn btn-danger btn-xs" type="submit">
                                    <span class="glyphicon glyphicon-trash"></span>
                                </button>
                            </form>

                        </div>
                    </td>
               </tr>

           {{-- Editar Usuario --}}

            <div class="modal inmodal" id="{{ $user->id }}" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content animated bounceInRight">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                        class="sr-only">Close</span></button>
                            <i class="fa fa-user modal-icon"></i>
                            <h4 class="modal-title">Editar Usuario</h4>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="/users/{{ $user->id }}" enctype="multipart/form-data"  >
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="username">Nombre Usuario</label>
                                    <input type="text" id="username" placeholder="Ingrese Nombre de la Usuario" class="form-control" name="username" value="{{ $user->name }}" >
                                </div>
                                <div class="form-group">
                                    <label for="email">Correo Usuario</label>
                                    <input type="email" id="email" placeholder="Ingrese Correo del Usuario" class="form-control" name="email" value="{{ $user->email }}" >
                                </div>
                                <div class="form-group">
                                    <label for="password">Contraseña</label>
                                    <input type="password" id="password" placeholder="Ingrese Contraseña del Usuario" class="form-control" name="password" required>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        @empty
            <h4>No hay usuarios registrados.</h4>
        @endforelse
    </table>

    {{-- Crear Usuario --}}

            <div class="modal inmodal" id="crear" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content animated bounceInRight">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                        class="sr-only">Close</span></button>
                            <i class="fa fa-user modal-icon"></i>
                            <h4 class="modal-title">Crear Usuario Administrador</h4>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="{{route('admin.create')}}" enctype="multipart/form-data"  >
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="username">Nombre Usuario</label>
                                    <input type="text" id="username" placeholder="Ingrese Nombre de la Usuario" class="form-control" name="name" value="" >
                                </div>
                                <div class="form-group">
                                    <label for="email">Correo Usuario</label>
                                    <input type="email" id="email" placeholder="Ingrese Correo del Usuario" class="form-control" name="email" value="" >
                                </div>
                                <div class="form-group">
                                    <label for="password">Contraseña</label>
                                    <input type="password" id="password" placeholder="Ingrese Contraseña del Usuario" class="form-control" name="password" required>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>

@endsection

@section('sidebar')
    @parent
@endsection


