<?php
/**
 * Created by PhpStorm.
 * User: Luis Marin
 * Date: 15/2/2019
 * Time: 00:40
 */
?>


@extends('layouts.app')

@section('title', 'Folletos')


@section('content')

    @if(Session::has('message'))
        <div class="alert {{ Session::get('alert-class', 'alert-info') }} alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <strong>{{ Session::get('message') }}</strong>
        </div>
        <!--<p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>-->
    @endif
    <div>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
            <i class="fa fa-upload"></i> Subir Catálogo
        </button>
        {{--    <!--<a href="{{ route('shop.create') }}" type="button" class="btn btn-primary">Agregar Tienda</a>-->--}}
    </div>
    <h1>Folletos</h1>

    <table class="table table-bordered">
        <thead class="text-center">
        <tr>
            <th>Tienda</th>
            <th>Catalogo Fecha</th>
            <th>Acción</th>
        </tr>
        </thead>
        @forelse ($brochure as $b)

            <tr>
                <td>{{ $b->shop }}</td>
                <td>{{ $b->created_at }}</td>
                <td>
                    <div class="col-md-6">
                        <button class="btn btn-xs btn-primary" data-toggle="modal" data-target="#{{ $b->id }}"><i class="fa fa-eye"></i></button>
                        {{--<button type="button" class="btn btn-xs" data-toggle="modal" data-target="#{{ $b->id }}"><span class="fa fa-edit"></span></button>--}}
                    </div>
                    <div class="col-md-6">
                        <form action="{{action('BrochureController@destroy', $b->id)}}" method="post"
                              onclick="return confirm('Seguro que desea eliminar el folleto?')">
                            {{csrf_field()}}
                            <input name="_method" type="hidden" value="DELETE">

                            <button class="btn btn-danger btn-xs" type="submit"><span
                                        class="glyphicon glyphicon-trash"></span></button>
                        </form>
                    </div>
                </td>
                </td>
                {{--{{ route('user.destroy',$user->id) }}--}}
            </tr>

            {{-- Editar Tienda --}}
            <div class="modal inmodal" id="{{ $b->id }}" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content animated bounceInRight">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                        class="sr-only">Close</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <iframe src="{{ url('/folletos', $b->name) }}" frameborder="0" width="100%" height="400px"></iframe>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">salir</button>
                        </div>
                    </div>
                </div>
            </div>
        @empty
            <h4>No hay Folletos registrados.</h4>
        @endforelse
    </table>



    <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                class="sr-only">Close</span></button>
                    <i class="fa fa-newspaper-o modal-icon"></i>
                    <h4 class="modal-title">Subir Catálogo</h4>
                </div>
                <div class="modal-body">
                    <form class="form-group" method="POST" action="/brochure" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Tienda</label>
                            <select name="name" id="name" class="form-control" required>
                                <option value="">------------  Seleccione una tienda  -----------</option>
                                @foreach($companies as $shop)
                                    <option value="{{ $shop->id }}">{{ $shop->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <input type="file" name="folleto" required>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>


@endsection

@section('sidebar')
    @parent
@endsection


