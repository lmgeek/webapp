<!DOCTYPE html>
<html lang="@yield('lang', config('app.locale', 'en'))">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Luis Marin">

  <title>@yield('title', config('app.name', 'Celta Camas y Sue&ntilde:os'))</title>

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Styles -->
  @section('styles')
  <link href="/webapp{{ mix('/css/inspinia.css') }}" rel="stylesheet">
  <link href="/webapp{{ asset('/css/style.css') }}" rel="stylesheet">
  <!--  Toastr -->
  <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">


  @show

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.3/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js">
  </script>
  <script scr="http://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.2/js/toastr.min.js">
  </script>
  <![endif]-->
  @stack('head')
</head>

<body class="body-small {{ config('inspinia.skin', '') }}">
  <div id="wrapper">
    @include('inspinia::layouts.sidebar.main')
    @include('inspinia::layouts.main-panel.main')
  </div>

  @section('scripts')
      <script
          src="https://code.jquery.com/jquery-3.3.1.min.js"
          integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
          crossorigin="anonymous"></script>
      <script src="/webapp{{ mix('/js/manifest.js') }}" charset="utf-8"></script>
      <script src="/webapp{{ mix('/js/vendor.js') }}" charset="utf-8"></script>
      <script src="/webapp{{ mix('/js/inspinia.js') }}" charset="utf-8"></script>
	@show
	@stack('body')
</body>

</html>
