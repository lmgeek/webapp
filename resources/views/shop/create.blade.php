<?php
/**
 * Created by PhpStorm.
 * User: luismarin
 * Date: 2019-02-03
 * Time: 14:08
 */

?>

@extends('layouts.app')


@section('content')
	<form class="form-group" method="POST" action="{{route('shop.store')}}" enctype="multipart/form-data">
		{{ csrf_field() }}
		<input type="file" name="avatar">
		<button type="submit">Subir</button>
	</form>
	<div class="jumborton">
		<form class="form-group" method="POST" action="{{route('shop.store')}}" enctype="multipart/form-data">
		<div class="row">
			<div class="col-md-6">
					{{ csrf_field() }}
					<div class="form-group">
					    <label for="name">Nombre Tienda</label>
					    <input type="text" id="name" placeholder="Ingrese Nombre de la Tienda" class="form-control" name="name" required >
					</div>
					<div class="form-group">
					    <label for="username">Nombre de Usuario</label>
					    <input type="text" id="username" placeholder="Ingrese nombre del usuario" class="form-control" name="username" required >
					</div>
					<div class="form-group">
					    <label for="password">Correo Tienda</label>
					    <input type="email" id="password" placeholder="Ingrese Correo del Usuario" class="form-control" name="email" required >
					</div>
					<div class="form-group">
					    <label>Contraseña</label>
					    <input type="password" placeholder="Ingrese Contraseña del Usuario" class="form-control" name="password" required >
					</div>
			</div>
			<div class="col-md-6">
				<img id="logo" alt="Logo de Tienda" width="200" height="200" src="{{ asset('/images/company/logo_empresa.png') }}"/>
				<br><br><br>
				<input type="file" name="avatar" onchange="document.getElementById('logo').src = window.URL.createObjectURL(this.files[0])" required>
			</div>
			<div class="col-md-12">
				<div class="modal-footer">
				    <button type="submit" class="btn btn-primary">Guardar</button>
				    <button type="button" class="btn btn-danger" onclick="history.back();">Cancelar</button>
				</div>
			</div>
		</div>
		</form>
	</div>
		
	

@endsection

@section('sidebar')
    @parent
@endsection





