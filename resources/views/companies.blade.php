<?php
/**
 * Created by PhpStorm.
 * User: luismarin
 * Date: 2019-02-03
 * Time: 14:46
 */
 $c_name = null;
?>

@extends('layouts.app')

@section('title', 'Tiendas')


@section('content')

    @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif
    <div>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
            Agregar Tienda
        </button>
    </div>
    <h1>Listado de Tiendas</h1>

    <table class="table table-bordered">
        <thead class="text-center">
        <tr>
            <th class="text-center">Logo</th>
            <th>Nombre</th>
            <!--<th>Catalogos</th>-->
            <th>Acciones</th>
        </tr>
        </thead>
        @forelse ($companies as $c)
        <?php
            $cat = DB::table('catalogues')
            ->select('name')
            ->where('company_id','=',$c->id)
            ->orderBy('created_at','desc')
            ->first();
            // $cat = array($catalogue);
        $echo = (array)$cat;
        foreach ($echo as $value) {
            $c_name = $value;
        }
        ?>

                <tr>
                    <td class="text-center"><img src="images/company/{{ $c->logo }}" width="60" height="60"></td>
                    <td>{{ $c->name }}</td>
                    <!--<td>
                        <button class="btn btn-xs btn-primary" data-toggle="modal" data-target="#Show-{{ $c->id }}" @if($cat==null) disabled @endif><i class="fa fa-eye"></i></button>
                        <!--<button class="btn btn-xs btn-primary" data-toggle="modal" data-target="#ModalUpload-{{ $c->id }}" ><i class="fa fa-upload"></i></button>
                    </td>-->
                    <td class="text-center">
                        <div class="col-md-6">
                            <button type="button" class="btn btn-xs" data-toggle="modal" data-target="#{{ $c->id }}"><span class="fa fa-edit"></span></button>
                        </div>
                        <form action="{{action('CompanyController@destroy', $c->id)}}" method="post" onclick="return confirm('Seguro que desea eliminar el usuario?')" >
                            {{csrf_field()}}
                            <input name="_method" type="hidden" value="DELETE">

                            <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-trash"></i></span></button>
                        </form>
                    </td>
                    </td>
                    {{--{{ route('user.destroy',$user->id) }}--}}
                </tr>

            {{-- Editar Tienda --}}
            <div class="modal inmodal" id="{{ $c->id }}" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content animated bounceInRight">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                    class="sr-only">Close</span></button>
                            <i class="fa fa-laptop modal-icon"></i>
                            <h4 class="modal-title">Editar Tienda</h4>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="{{route('shop.update', $c->id) }}" enctype="multipart/form-data"  >
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="name">Nombre Tienda</label>
                                    <input type="text" id="name" placeholder="Ingrese Nombre de la Tienda" class="form-control" name="name" value="{{ $c->name }}" >
                                </div>
                                <div class="form-group">
                                    <label for="email">Correo Tienda</label>
                                    <input type="email" id="email" placeholder="Ingrese Correo del Usuario" class="form-control" name="email" value="{{ $c->email }}" >
                                </div>
                                <input type="file" name="avatar" onchange="document.getElementById('logo').src = window.URL.createObjectURL(this.files[0])">
                                <img id="logo" alt="Logo de Tienda" width="100" height="100" src="/webapp/images/company/{{ $c->logo }}"/>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            {{-- Modal Editar --}}

            {{-- ModalUpload --}}
            <div class="modal inmodal" id="ModalUpload-{{ $c->id }}" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content animated bounceInRight">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                        class="sr-only">Close</span></button>
                            <i class="fa fa-newspaper-o modal-icon"></i>
                            <h4 class="modal-title">Subir Catálogo</h4>
                        </div>
                        <div class="modal-body">
                            <form method="POST" action="{{route('catalogue.store')}}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" id="name" placeholder="Ingrese Nombre de la Tienda" class="form-control" name="name" value="{{ $c->id }}" >
                                <div class="form-group">
                                    <label for="name">Tienda</label>
                                    <input type="text" id="name" readonly class="form-control" value="{{ $c->name }}" >
                                </div>
                                <input type="file" name="catalogo" required>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            {{-- ModalUpload --}}

            {{-- Show Tienda --}}
            <div class="modal inmodal" id="Show-{{ $c->id }}" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content animated bounceInRight">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                                        class="sr-only">Close</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <iframe src="{{ url('/catalogos', $c_name) }}#zoom=100&view=fitH" frameborder="0" width="100%" height="400px"></iframe>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">salir</button>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Show Tienda --}}
        @empty
            <h4>No hay tiendas registradas.</h4>
        @endforelse
    </table>



    <div class="modal inmodal" id="myModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content animated bounceInRight">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
                    <i class="fa fa-laptop modal-icon"></i>
                    <h4 class="modal-title">Agregar Tienda</h4>
                </div>
                <div class="modal-body">
                    <form class="form-group" method="POST" action="{{route('shop.store')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="name">Nombre Tienda</label>
                            <input type="text" id="name" placeholder="Ingrese Nombre de la Tienda" class="form-control" name="name" required>
                        </div>
                        <!-- <div class="form-group">
                            <label for="username">Nombre de Usuario</label>
                            <input type="text" id="username" placeholder="Ingrese nombre del usuario" class="form-control" name="username" required>
                        </div> -->
                        <div class="form-group">
                            <label for="email">Correo Tienda</label>
                            <input type="email" id="email" placeholder="Ingrese Correo del Usuario" class="form-control" name="email" required>
                        </div>
                        <div class="form-group">
                            <label for="password">Contraseña</label>
                            <input type="password" id="password" placeholder="Ingrese Contraseña del Usuario" class="form-control" name="password" required>
                        </div>
                        <input type="file" name="avatar" onchange="document.getElementById('logo').src = window.URL.createObjectURL(this.files[0])" required>
                        <img id="logo" alt="Logo de Tienda" width="100" height="100" src="{{ asset('/images/logo_empresa.jpg') }}"/>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>




    {{--<script>--}}
            {{--@if(Session::has('message'))--}}
        {{--var type = '{{Session::get('alert-type','info')}}'--}}

        {{--switch (type) {--}}
            {{--case 'info':--}}
                {{--toastr.info("{{ Session::get('message') }}");--}}
                {{--break;--}}
            {{--case 'success':--}}
                {{--toastr.info("{{ Session::get('message') }}");--}}
                {{--break;--}}
            {{--case 'update':--}}
                {{--toastr.info("{{ Session::get('message') }}");--}}
                {{--break;--}}
            {{--case 'error':--}}
                {{--toastr.info("{{ Session::get('message') }}");--}}
                {{--break;--}}
        {{--}--}}
        {{--@endif--}}
    {{--</script>--}}
@endsection

@section('sidebar')
    @parent
@endsection

