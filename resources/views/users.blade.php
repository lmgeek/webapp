<?php
/**
 * Created by PhpStorm.
 * User: luismarin
 * Date: 2019-02-03
 * Time: 14:08
 */

?>

@extends('layouts.app')


@section('content')

    <form class="form-group" method="POST" action="/file2" enctype="multipart/form-data">
		{{ csrf_field() }}
		<input type="file" name="avatar">
		<button type="submit">Subir</button>
	</form>
@endsection

@section('sidebar')
    @parent
@endsection





