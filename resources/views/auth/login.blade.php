@extends('inspinia::layouts.auth')

@section('content')
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
      <div><h1 class="logo-name"><img src="/webapp/images/logo.jpg" alt=""></h1></div>
      <form class="m-t" role="form" method="POST" action="{{ route('login') }}">
       {{ csrf_field() }}
       <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <input type="email" class="form-control" placeholder="E-Mail" name="email" value="{{ old('email') }}" required autofocus>
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
       </div>
       <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <input type="password" class="form-control" placeholder="Password" name="password" required>
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
       </div>
       {{-- <div class="form-group">
         <div class="checkbox i-checks">
           <label> <input type="checkbox" name="remember" value="1" {{ old('remember') ? 'checked' : '' }}><i></i> Aceptar Terminos y Condiciones </label>
         </div>
       </div> --}}
       <button type="submit" class="btn btn-primary block full-width m-b">Iniciar Sesión</button>

       <!--<a href="{{ route('password.request') }}"><small>Olvide mi Contraseña</small></a>
       <p class="text-muted text-center"><small>¿No tienes una cuenta?</small></p>
       <a class="btn btn-sm btn-white btn-block" href="{{ route('register') }}">Crear Cuenta</a>-->
      </form>
      <p class="m-t"> &copy; {{ date('Y') }} <strong>Administrador de Catálogos <br>y APP Móvil<!--</strong> <br><small>Create with <i class="fa fa-heart" style="color: red"></i> by <a href="//marinluis.com">Luis Marín</a></small>--></p>
    </div>
</div>
@endsection
