<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/users', 'HomeController@users')->name('users');
Route::get('/useradmin', 'HomeController@admin')->name('admin');
Route::post('/users/{id}', 'UserController@update')->name('users.updates');
Route::post('/useradmin/create', 'UserController@create')->name('admin.create');
Route::delete('/users/{id}', 'UserController@destroy')->name('users.destroy');

//Route::resource('company', 'CompanyController');

Route::get('shop','CompanyController@index')->name('shop.index');
Route::post('shop','CompanyController@store')->name('shop.store');
Route::get('reporte', 'CompanyController@excel')->name('report.excel');

Route::post('shop/{shop}','CompanyController@update')->name('shop.update');
Route::delete('shop/{shop}','CompanyController@destroy')->name('shop.destroy');

Route::get('/file','HomeController@file')->name('file.show');
Route::post('/file2','HomeController@file2')->name('file.save');

//Route::resource('shop', 'ShopController');
//Route::post('/shop/save', 'ShopController@save');

//catalogos rutas
Route::resource('catalogue','CatalogueController', [
    'names' => [
        'index' => 'catalogue.index',
        'create' => 'catalogue.create',
        'store' => 'catalogue.store',
        'show' => 'catalogue.show',
        'edit' => 'catalogue.edit',
        'update' => 'catalogue.update',
        'destroy' => 'catalogue.destoy'
    ]
]);

//catalogos folletos
Route::resource('brochure','BrochureController', [
    'names' => [
        'index' => 'brochure.index',
        'create' => 'brochure.create',
        'store' => 'brochure.store',
        'show' => 'brochure.show',
        'edit' => 'brochure.edit',
        'update' => 'brochure.update',
        'destroy' => 'brochure.destoy'
    ]
]);